[![SWUbanner]][SWUdocs]

[SWUbanner]:
https://raw.githubusercontent.com/vshymanskyy/StandWithUkraine/main/banner-personal-page.svg
[SWUdocs]:
https://github.com/vshymanskyy/StandWithUkraine/blob/main/docs/README.md


### Hi there 👋

- 🔭 I’m currently working on [ansible/ansible](https://github.com/ansible/ansible)
  by day 🌅 and when the sun goes down 🌇 I do [all sorts of other FOSS
  things](https://github.com/sponsors/webknjaz)
- 🌱 I’m currently learning [CPython Internals](
https://realpython.com/products/cpython-internals-book/)
  and [the basics of OpenShift](
  https://www.redhat.com/en/services/training/do180-introduction-containers-kubernetes-red-hat-openshift)
- 👯 I’m looking to collaborate on improving the Python ecosystem around
  making GitHub Apps and Actions, packaging, testing, CI/CD
- 🤔 I’m looking for help with getting more hours in a day
- 💬 Ask me about Python packaging, CI/CD, pytest, creating GitHub Apps,
  GitHub Actions, Ansible Collections, CherryPy internals,
  async in Python 3, tox, Gentoo Linux, Git, code reviews, linters,
  maintaining the compatibility with legacy Pythons, dev env, burnouts
- 📫 How to reach me: tweet
- 😄 Pronouns: [he/him](http://pronoun.is/he)
- ⚡ Fun fact: wanting to "fix" a game got me interested in computers
  when I was 7 but when I got older I lost interest in games, worked in
  gaming for just 3 months and never looked back
- Oh, and also I'm a

  [![Contact me on Codementor](https://www.codementor.io/m-badges/webknjaz/book-session.svg)](https://www.codementor.io/@webknjaz?refer=badge)

### My ramblings 📝
- [ansible-galaxy CLI ❤️ resolvelib _(2021-02-17)_](https://webknjaz.me/prose/ansible-galaxy-reuses-pips-resolvelib/)
- [Et Tu Brutè? Use Travis CI for FOSS no more. _(2020-11-13)_](https://webknjaz.me/prose/et-tu-brute-use-travis-ci-for-foss-no-more/)
- [Hello Website. Again. _(2020-11-01)_](https://webknjaz.me/prose/brave-new-world/)
